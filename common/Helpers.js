const isValidUrl = (originalUrl, callbackFn) => {
    const dns = require('dns');
    var url = require("url");
    var parsedUrl = url.parse(originalUrl);
    // if url does not have format http(s)://www.domainname.com return error
    if (!parsedUrl.protocol && !parsedUrl.slashes && !parsedUrl.hostname) {
        return callbackFn({error: true});
    }
    dns.lookup(parsedUrl.hostname, (err) => {
        callbackFn(err);
    });
};

const handleError = function (err, req, res, next) {
    console.error(err.stack)
    res.status(500).send('Something broke!');
}  

exports.Helpers = {
    isValidUrl,
    handleError
};