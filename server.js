'use strict';

var express = require('express');
var mongo = require('mongodb');
var mongoose = require('mongoose');
mongoose.Promise = Promise;
var bodyParser = require('body-parser');
require('dotenv').config()

var cors = require('cors');

var app = express();
const ShortUrl = require('./models/ShortUrl').ShortUrl;
const Helpers = require('./common/Helpers').Helpers;


// Basic Configuration 
var port = process.env.PORT || 3000;

/** this project needs a db !! **/ 
mongoose.connect(process.env.MONGOLAB_URI,  { useMongoClient: true });

app.use(Helpers.handleError);
app.use(cors());

/** this project needs to parse POST bodies **/
// you should mount the body-parser here
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/public', express.static(process.cwd() + '/public'));

app.get('/', function(req, res){
  res.sendFile(process.cwd() + '/views/index.html');
});

  
// your first API endpoint... 
app.get("/api/hello", function (req, res) {
  res.json({greeting: 'hello API'});
});

app.get("/api/shorturl/:shortUrl", async function (req, res) {
  const result = await ShortUrl.findOne({ shortUrl: req.params.shortUrl });
  if (!result) {
    return res.json({ error: 'No record found for short url.'});
  }
  res.redirect(result.originalUrl);
});

app.post("/api/shorturl/new", async function (req, res) {
  const passedUrl = req.body.url;

  Helpers.isValidUrl(passedUrl, async (err) => {
    if (err) {
      return res.json({ "error": "invalid URL" });
    }
    const countResult = await ShortUrl.count({});
    const shortUrl = new ShortUrl({ originalUrl: passedUrl, shortUrl: countResult + 1 });
    const saveResult = await shortUrl.save();
    if (!saveResult) {
      return res.json({error: 'An error occured while saving record.'});
    }
    res.json({
      "original_url": saveResult.originalUrl,
      "short_url": saveResult.shortUrl
    });
  });
});

app.listen(port, function () {
  console.log('Node.js listening ...', port);
});