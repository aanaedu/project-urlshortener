const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ShortUrlSchema = new Schema({
    originalUrl: { type: String, required: true },
    shortUrl: { type: Number, required: true }
});

const ShortUrl = mongoose.model('Url', ShortUrlSchema);

exports.ShortUrl = ShortUrl;